import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { BrowserRouter as Router } from 'react-router-dom'
import { init } from './services/MusicSearch';
import { UserProvider } from './services/UserService';
init()

ReactDOM.render(
  <React.StrictMode>
    <UserProvider>
      <Router>
        <App />
      </Router>
    </UserProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();


// import React from 'react';
// import ReactDOM from 'react-dom';
// import RatingWidget from './components/RatingWidget';

// // Expose 
// console.log('Rating widget Loaded')

//   ; (window as any).RatingWidget = RatingWidget
//   ; (window as any).React = React
//   ; (window as any).ReactDOM = ReactDOM


// const jQuery = (window as any).jQuery;

// if (jQuery) {

//   jQuery.fn.rating = function (props) {

//     this.each((index, elem) => {

//       elem.jQueryRatingData = { ...elem.jQueryRatingData, ...props };

//       const jsx = React.createElement(RatingWidget, elem.jQueryRatingData)

//       ReactDOM.render(jsx, elem)
//     })
//   }

//   // $('#root').rating({ stars:6, rating:3, onChange:console.log})
//   // $('#root').rating({ stars:6})
//   // $('#root').rating({ rating:6})
// }
