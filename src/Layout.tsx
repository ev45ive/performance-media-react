import React, { useState } from 'react';
import { Link, NavLink } from 'react-router-dom'
import { UserCard } from './services/UserService';

// const MojLink = ({ children, to, ...restProps }) => <a {...restProps} onClick={e => e.preventDefault(); history.pushState(to)}> { children }</a >
// const MojLink = ({activeClassName,...props}) => <NavLink {...props} activeClassName={"placki "+activeClassName} />

export const Layout: React.FC<any> = (props) => {
  const [expanded, setexpanded] = useState(false);

  return <>

    <nav className="navbar navbar-expand-md navbar-dark bg-dark mb-3">
      <div className="container">

        <NavLink className="navbar-brand" to="/">MusicApp</NavLink>

        <button className="navbar-toggler" type="button" onClick={() => setexpanded(!expanded)}>
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className={`collapse navbar-collapse ${expanded ? 'show' : ''} `}>
          <ul className="navbar-nav">

            <li className="nav-item">
              <NavLink className="nav-link" activeClassName="active placki" to="/playlists">Playlists</NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link" activeClassName="active placki" to="/search">Search</NavLink>
            </li>

          </ul>
          <span className="navbar-text ml-auto">
            <UserCard />
          </span>
        </div>
      </div>
    </nav>

    <div className="container">
      {props.children}
    </div>
  </>;
};
