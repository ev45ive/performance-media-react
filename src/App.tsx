import React from 'react';
// import logo from './logo.svg';
import './App.scss';
import 'bootstrap/dist/css/bootstrap.css';
import SearchView from './views/SearchView';
import { Layout } from './Layout';
import PlaylistsView from './views/PlaylistsView';

/* Router */
import { Route, HashRouter as Router, Switch, Redirect } from 'react-router-dom'
import { AlbumDetailsView } from './views/AlbumDetailsView';

function App() {
  return (
    <Layout>
        <Switch>
          <Redirect path="/" exact={true} to="/playlists" />
          <Route path="/playlists" component={PlaylistsView} />
          <Route path="/search" component={SearchView} />
          <Route path="/album/:album_id" component={AlbumDetailsView} />
          <Route path="*" render={() => <h1>404 Page Not Found</h1>} />
        </Switch>
    </Layout>
  );
}

export default App;
