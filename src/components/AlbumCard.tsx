import React from 'react';

export const AlbumCard = ({ result }) => {
  return (
    <div className="card">

      <img src={result.images[0].url} className="card-img-top" />

      <div className="card-body">
        <h5 className="card-title">{result.name}</h5>
      </div>

    </div>
  );
};
