import React, { Component, PureComponent } from 'react'
import { Playlist } from './Playlist'

type P = {
  playlists: Playlist[],
  selected: Playlist | null,
  onSelectPlaylist: (playlist: any) => void
}

export default class PlaylistsList extends PureComponent<P> {

  select(playlist) {
    this.props.onSelectPlaylist(playlist)
  }

  render() {
    return (
      <div>
        <div className="list-group">
          {
            this.props.playlists.map(
              (playlist, index) => (
                <div className={"list-group-item " +
                  (this.props.selected &&
                  this.props.selected &&
                    this.props.selected.id === playlist.id ? 'active' : '')}

                  key={playlist.id}
                  onClick={() => this.select(playlist)}>
                  {index + 1}.  {playlist.name}
                </div>
              )
            )
          }

        </div>

      </div >
    )
  }
}
