import React, { Component } from 'react'
import { UserCard } from '../../services/UserService'

type PropTypes = {
  playlist: {
    name: string;
    public: boolean;
    description: string;
  },
  onClickEdit: () => void
}


export default class PlaylistDetails extends Component<PropTypes> {

  render() {
    return (
      <div>
        {/* dl>(dt+dd)*3 */}
        <dl>
          <dt>Name:</dt>
          <dd>{this.props.playlist.name}</dd>

          <dt>Public:</dt>
          <dd>{this.props.playlist.public ? 'Yes' : 'No'}</dd>

          <dt>Description:</dt>
          <dd>{this.props.playlist.description}</dd>
        </dl>
        {/* button.btn.btn-info{Edit} */}

        <button className="btn btn-info" 
            onClick={this.props.onClickEdit}>Edit</button>

          <UserCard />
      </div>
    )
  }
}
