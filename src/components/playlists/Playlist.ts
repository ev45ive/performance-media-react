
export type Playlist = {
  id: number;
  name: string;
  public: boolean;
  /**
   * Dokumentacja
   * @author placki
   */
  description: string;
};
