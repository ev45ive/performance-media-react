
import React, { Component, PureComponent } from 'react'
import { Playlist } from './Playlist';

type P = {
  playlist: Playlist,
  onClickCancel: () => void,
  onClickSave: (draft: Playlist) => void
}

export default class PlaylistForm extends PureComponent<P, any> {

  state = {
    playlist: {
      id: 123,
      name: 'React HITS',
      public: true,
      description: 'opis..'
    }
  }

  handleChange = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value

    // this.state.playlist[target.name] = value
    // this.setState({})

    this.setState({
      playlist: {
        ...this.state.playlist,
        [target.name]: value
      }
    })
  }

  inputRef = React.createRef<HTMLInputElement>()

  render() {
    console.log('render')
    return (
      <div>
        {JSON.stringify(this.state.playlist)}

        <div className="form-group">
          <label>Name:</label>
          <input type="text" className="form-control" ref={this.inputRef}
            value={this.state.playlist.name} name="name"
            onChange={this.handleChange} />

          {170 - this.state.playlist.name.length} / 170
        </div>

        <div className="form-group">
          <label> <input type="checkbox" name="public" checked={this.state.playlist.public}
            onChange={this.handleChange} /> Public</label>
        </div>

        <div className="form-group">
          <label>Description:</label>
          <textarea className="form-control" name="description" value={this.state.playlist.description}
            onChange={this.handleChange} />
        </div>

        {/* button.btn.btn-danger{Cancel} */}
        <button className="btn btn-danger"
          onClick={this.props.onClickCancel}>Cancel</button>

        <button className="btn btn-success"
          onClick={
            () => {
              debugger;
              this.props.onClickSave(this.state.playlist)
            }
          }>Save</button>
      </div>
    )
  }

  constructor(props) {
    super(props)
    console.log('constructor')
    // this.state.playlist = this.props.playlist
  }

  static getDerivedStateFromProps(newProps, newState) {
    // console.log('getDerivedStateFromProps');
    return {
      playlist:

        newState.playlist.id == newProps.playlist.id ?

          newState.playlist : newProps.playlist
    }
  }

  componentDidMount() {
    console.log('componentDidMount')
    this.inputRef.current.focus()
  }

  componentWillUnmount() { console.log('componentWillUnmount') }

  // shouldComponentUpdate(nextProps, nextState) {
  //   console.log('shouldComponentUpdate');/*  return false */
  //   return this.props.playlist !== nextProps.playlist || this.state.playlist !== nextState.playlist
  // }

  getSnapshotBeforeUpdate() { console.log('getSnapshotBeforeUpdate'); return {} }

  componentDidUpdate() { console.log('componentDidUpdate') }
}


// PlaylistForm. getDerivedStateFromProps() { console.log('getDerivedStateFromProps'); return {}}
