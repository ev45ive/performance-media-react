import React from 'react';
import { AlbumCard } from './AlbumCard';
import { Link } from 'react-router-dom'

// https://getbootstrap.com/docs/4.5/components/card/#grid-cards

export const SearchResults = ({ results }: any) => {
  // const { results } = props

  return (
    <div>
      <div className="row row-cols-1 row-cols-md-2">
        {
          results.map(result => <div className="col mb-4" key={result.id}>
            <Link to={'/album/' + result.id}> <AlbumCard result={result} /> </Link>
          </div>)
        }
      </div>
    </div>
  )
};



