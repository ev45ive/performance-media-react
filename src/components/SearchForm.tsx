import React, { useState, useEffect, useRef } from 'react';


export const SearchForm = (props) => {
  const [query, setQuery] = useState('')
  const [type, setType] = useState('album')
  const [valid, setValid] = useState({ ok: false, errors: [] })
  const isFirst = useRef(true)
  const queryRef = useRef(null)

  useEffect(() => {
    queryRef.current.focus()
  }, [])

  useEffect(() => {
    setQuery(props.query)
  }, [props.query])

  useEffect(() => {
    if (isFirst.current) { isFirst.current = false; return }

    const handler = setTimeout(() => props.onSearch(query), 2000)
    return () => clearInterval(handler)
  }, [query])

  return (
    <div>
      <div className="input-group mb-3">

        <input type="text" className="form-control" placeholder="Search" ref={queryRef}
          value={query}
          onChange={e => setQuery(e.target.value)}
          onKeyUp={e => e.key === 'Enter' && props.onSearch(query)} />

        {/* <select className="form-control" value={type} onChange={e => setType(e.target.value)}>
          <option value="album">Album</option>
          <option value="artist">Artist</option>
        </select> */}

        <div className="input-group-append">
          <button className="btn btn-outline-secondary" onClick={() => props.onSearch(query)}>
            Search
          </button>
        </div>

      </div>
      {170 - query.length} / 170
    </div>
  );

};
