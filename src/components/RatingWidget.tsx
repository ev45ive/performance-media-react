import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';

const Star = React.memo<any>(
  ({ empty = true, ...restProps }) => (

    /* pass ALL ...props from component to HTMLElement  ...props */
    <span {...restProps}>{
      empty ? <span>&#x2606;</span> : <span>&#9733;</span>
    }</span>
  )
)

export const RatingWidget = (props) => {
  const [rating, setRating] = useState(props.rating || 0)

  useEffect(() => {
    setRating(props.rating)
  }, [props.rating])

  const stars = Array.from(Array(props.stars || 5).keys())

  const changeRating = (rating = 0) => {
    setRating(rating);
    props.onChange && props.onChange(rating)
  }

  return (
    <div>
      Rating
      {stars.map((star, index) => <Star
        empty={!(rating > index)}
        key={index}
        /*  */
        onClick={() => changeRating(index + 1)}
        className="text-warning btn px-0"
        title={index + 1}
      />
      )}
    </div>
  );
};

export default RatingWidget;

