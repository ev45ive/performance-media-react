import React, { Component } from 'react'
import PlaylistsList from '../components/playlists/PlaylistsList'
import PlaylistDetails from '../components/playlists/PlaylistDetails'
import PlaylistForm from '../components/playlists/PlaylistForm'
import { Playlist } from '../components/playlists/Playlist'

export default class PlaylistsView extends Component {

  state = {
    selected: {
      id: 123,
      name: 'React Hits',
      public: true,
      description: 'opis..'
    },

    playlists: [
      {
        id: 123,
        name: 'React Hits 123',
        public: true,
        description: 'opis..'
      },
      {
        id: 234,
        name: 'React Hits 234',
        public: false,
        description: 'opis.. 234'
      }, {
        id: 345,
        name: 'React Hits 345',
        public: true,
        description: 'opis..'
      }
    ],

    mode: 'show' /* show || edit || 'preview' || 'craete' || ... */
  }

  select = (playlist: Playlist) => {
    this.setState({
      selected: playlist,
    })
  }

  edit = () => {
    this.setState({ mode: 'edit' })
  }

  cancel = () => {
    this.setState({ mode: 'show' })
  }

  save = (draft) => {
    // debugger;
    // const playlists = this.state.playlists
    // const index = playlists.findIndex(p => p.id === draft.id)
    // playlists.splice(index, 1, draft)

    const playlists = this.state.playlists.map(p => p.id === draft.id? draft: p)

    this.setState({
      mode: 'show', 
      selected: draft, 
      playlists: playlists
    })
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <PlaylistsList
              selected={this.state.selected}
              playlists={this.state.playlists}
              onSelectPlaylist={this.select} />
          </div>

          <div className="col">
            {this.state.mode === 'show' ?
              <PlaylistDetails
                onClickEdit={this.edit}
                playlist={this.state.selected} /> : null}

            {this.state.mode === 'edit' && <PlaylistForm
              onClickSave={this.save}
              onClickCancel={this.cancel}
              playlist={this.state.selected} />}
          </div>
        </div>
      </div>
    )
  }
}
