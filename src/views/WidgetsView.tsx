// rafc 
import React, { useState, Suspense } from 'react'

// import { RatingWidget } from '../views/RatingWidget'

const RatingWidget = React.lazy(() => import('../components/RatingWidget')
  // .then(m => m.RatingWidget) // export default RatingWidget
)

export const WidgetsView = () => {
  const [isOpen, setIsOpen] = useState(false)
  const [rating, setRating] = useState(3)

  return (
    <div>
      <button onClick={() => setIsOpen(!isOpen)}>Load Widget</button>

      {isOpen ? 'Yes' : 'No'}

      {isOpen && <>

        Dynamicaly loaded component:

        <br />
        <Suspense fallback={<p> Loading ... </p>}>
          <RatingWidget onChange={setRating} />
          <RatingWidget stars={3} rating={rating} />
          <RatingWidget stars={5} rating={rating} />
          <RatingWidget stars={10} rating={rating} />
        </Suspense>

      </>}

    </div>
  )
}



