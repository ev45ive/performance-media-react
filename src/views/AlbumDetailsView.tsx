
import React, { useState, useEffect, useRef } from 'react'
import { AlbumCard } from '../components/AlbumCard'
import { useParams } from 'react-router-dom'
import { getAlbumById } from '../services/MusicSearch'

export const AlbumDetailsView = () => {
  const [album, setAlbum] = useState(null)
  const [curentTrack, setCurrentTrack] = useState(null)
  const { album_id } = useParams()
  const audioRef = useRef(null)

  useEffect(() => {
    getAlbumById(album_id).then(album => {
      setAlbum(album)
    })
  }, [album_id])

  useEffect(() => {
    if (!curentTrack) { return }
    audioRef.current.volume = 0.2;
    audioRef.current.src = curentTrack.preview_url;
    audioRef.current.play()
  }, [curentTrack])

  const play = (track) => {
    setCurrentTrack(track)
  }

  return (
    <div>
      {/* {album_id} */}
      {
        album ?
          <div className="row">
            <div className="col">
              <AlbumCard result={album} />
            </div>
            <div className="col">
              <div className="list-group">
                <audio controls={true} className="w-100" ref={audioRef} />

                {album.tracks.items.map(item =>
                  <div className={"list-group-item " + (item == curentTrack ? 'active' : '')} key={item.id}>
                    {item.name}
                    <span className="float-right" onClick={() => play(item)}>Play</span>
                  </div>
                )}
              </div>

            </div>
          </div> : <p>Loading... </p>
      }

    </div>
  )
}
