import React, { useState, useEffect } from 'react'
import { SearchForm } from '../components/SearchForm'
import { SearchResults } from '../components/SearchResults'
import { searchAlbums } from '../services/MusicSearch'
import { useLocation, useHistory } from 'react-router-dom'

export const SearchView = () => {
  const [results, setResults] = useState([])
  const [message, setMessage] = useState('')
  const { search } = useLocation()
  const { push } = useHistory()

  const queryParams = new URLSearchParams(search.slice(1))
  const query = queryParams.get('q') || 'alice';

  const fetchResults = (query) => {
    push('/search?q=' + query)
  }

  useEffect(() => {
    setResults([]);
    // if (!query) { return; }
    
    searchAlbums(query).then(results => setResults(results.albums.items))
      .catch(error => setMessage(error.message))
  }, [query])

  return (
    <div>
      <div className="row">
        <div className="col">
          <SearchForm onSearch={fetchResults} query={query} />
        </div>
      </div>
      <div className="row">
        <div className="col">
          {message}
          <SearchResults results={results} />
        </div>
      </div>
    </div>
  )
}

export default SearchView