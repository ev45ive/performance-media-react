import React, { useContext, useState, FC, useEffect } from "react";
import { getToken } from "./MusicSearch";

export const UserContext = React.createContext<any>({
  isLoggedIn: false,
  user: null,
  login: () => { throw Error('No Provider for UserContext') },
  logout: () => { throw Error('No Provider for UserContext') },
})

export const UserCard = () => {
  const { isLoggedIn, user, login, logout } = useContext(UserContext)

  return (
    <span>
      {isLoggedIn ? user && user.display_name : 'Anonym'}

      {isLoggedIn ?
        <span onClick={logout}>, Logout</span>
        :
        <span onClick={login}>, Login</span>}

    </span >
  )
}

export const UserProvider: FC<any> = ({ children }) => {
  const [isLoggedIn, setLoggedIn] = useState(false)
  const [user, setUser] = useState(null)

  const login = () => {
    fetch('https://api.spotify.com/v1/me', {
      headers: { Authorization: 'Bearer ' + getToken() }
    }).then(resp => resp.json())
      .then(user => {
        setLoggedIn(true)
        setUser(user)
      })

  }
  const logout = () => {
    setLoggedIn(false)
    setUser(null)
  }

  const token = getToken()
  useEffect(() => {
    if (token) {
      login()
    }
  }, [token])


  return <UserContext.Provider value={{
    isLoggedIn, login, logout, user
  }}>
    {children}
  </UserContext.Provider>
}