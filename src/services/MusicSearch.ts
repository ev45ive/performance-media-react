
export const getAlbumById = (id) => {

  return fetch('https://api.spotify.com/v1/albums/' + id, {
    headers: { Authorization: 'Bearer ' + getToken() }
  }).then(resp => resp.json())
}

export const searchAlbums = (query) => {

  const params = new URLSearchParams({ q: query, type: 'album' })

  const resp = fetch('https://api.spotify.com/v1/search?' + params.toString(), {
    headers: { Authorization: 'Bearer ' + getToken() }
  })

  return resp.then(resp => resp.json())
    .then(resp => resp.error ? Promise.reject(resp.error) : resp)
    // Login again when token expired
    .catch(error => {
      if (error.status === 401) {
        authorize()
        return []
      } else {
        return Promise.reject(error)
      }
    })
}

let token: string | null = null;

export const init = () => {
  token = JSON.parse(sessionStorage.getItem('token'))

  if (!token) {
    const p = new URLSearchParams(window.location.hash)
    token = p.get('#access_token')
  }

  if (!token) {
    authorize()
  } else {
    sessionStorage.setItem('token', JSON.stringify(token))
  }
}


export const authorize = () => {
  sessionStorage.removeItem('token')

  const url = 'https://accounts.spotify.com/authorize';

  const params = new URLSearchParams({
    client_id: 'a4e58056c5b044aea481a5388d10f3bd',
    response_type: 'token',
    redirect_uri: 'http://localhost:3000/'
  })

  window.location.href = (`${url}?${params.toString()}`)
  // console.log((`${url}?${params.toString()}`))
}

export const getToken = () => {
  return token;
}

// searchAlbums('batman')
//   .then(console.log)
//   .catch(console.error)

//   searchAlbums('superman')
//   .then(console.log)
//   .catch(console.error)

// searchAlbums('catwoman')
//   .then(console.log)
//   .catch(console.error)