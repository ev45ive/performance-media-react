```js
function echo(msg, cb){

    setTimeout(()=> cb(msg) , 2000)
}

echo('placki', r => {
    echo( 'lubie '+ r, r2 => {
        echo('bardzo ' +r2  , console.log)
    })    
}) 
```
=========
```js
function echo(msg){
    return new Promise((resolve)=>{
        setTimeout(()=> resolve(msg) , 2000)
    }) 
}

p = echo('placki')


p
.then(r => echo( 'lubie ' +r) )
.then(console.log)

p
.then(r => ' super '+r)
.then(console.log) 
```
=====
```js
params = new URLSearchParams({q:'batman',type:'album'})

resp = fetch('https://api.spotify.com/v1/search?'+params.toString(),{
    headers:{ Authorization:'Bearer placki' }
})

resp.then(resp => resp.json())
.then( resp => resp.error ? Promise.reject(resp.error) : resp)

.then(console.log) 
.catch(console.error)
```