# GIT
cd ..
git clone https:// bitbucket.org/ ev45ive/ performance-media-react.git

# oAuth
placki234@placki.com
placki234

# Sudo free living
npm config set prefix ~/.npm

# Install create React App
npx create-react-app  nazwa_katalogu_projektu 
npx create-react-app  nazwa_katalogu_projektu --template typescript

<!-- lub -->

npm i -g create-react-app 
create-react-app nazwa_katalogu_projektu --template typescript
cd react-pm
npm start

cd ..
sudo chmod -R 777 react-pm
npm i bootstrap
npm i node-sass

# VS Code
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

# Prop types (runtime checking)
https://pl.reactjs.org/docs/typechecking-with-proptypes.html

# Strict Mode
https://pl.reactjs.org/docs/strict-mode.html#detecting-unexpected-side-effects

# React router
npm i react-router-dom

# SEO / ServerSideREndering
https://create-react-app.dev/docs/pre-rendering-into-static-html-files

Next.js / Gatsby.js


# Zapraszam do kontaktu

https://www.linkedin.com/in/mateuszkulesza/

https://www.linkedin.com/posts/max-rud-677998123_client-product-improvise-activity-6689805413124206592-iu4t